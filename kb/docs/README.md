![Open Banking Logo](https://bitbucket.org/openbankingteam/conformance-suite/raw/99b76db5f60bb4d790d6f32bffae29cbe95a3661/docs/static_files/OBIE_logotype_blue_RGB.PNG)
---

Welcome to the open source **Knowledge Base for Open Banking**. This repository contains the markdown files *(with the .MD extension)* used across Open Banking and all the associated services. If you want to use markdown files then you don't need to run this code, we offer a hosted version of the app at [INSERT OB LINK TO CONFLUENCE]. If you'd like to host your own copy of Knowledge Base or contribute, then this is the place for you.

---

## Contributing and Improving the Knowledge Base

#
## How to add a new Question

* Create a page/folder in [Bitbucket](https://bitbucket.org/openbankingteam/knowledge-base/src/master/)
* Create a page in Confluence
* Add Bitbucket Markdown macro and add the Bitbucket URL
  