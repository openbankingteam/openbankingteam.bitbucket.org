# Where do I download the Conformance Tool?

The functional conformance tool can be downloaded as an image from DockerHub or run as source code from Bitbucket. 

* [DockerHub](https://cloud.docker.com/u/openbanking/repository/docker/openbanking/conformance-suite)
* [Bitbucket](https://bitbucket.org/openbankingteam/conformance-suite/)