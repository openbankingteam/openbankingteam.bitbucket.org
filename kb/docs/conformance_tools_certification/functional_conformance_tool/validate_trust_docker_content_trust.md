# How to I validate and trust the Conformance Tool image I'm downloading? (Docker Content Trust)

Docker Content Trust (DCT) allows Open Banking to sign docker images before they get published to the Docker Hub registry. These signatures allow client-side or runtime verification of the integrity and publisher of specific image tags.

You can find full details on how to validate [here](https://bitbucket.org/openbankingteam/conformance-suite/src/develop/docs/docker_content_trust.md)