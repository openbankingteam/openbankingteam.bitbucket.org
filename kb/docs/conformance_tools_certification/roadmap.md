# Conformance Tool & Dynamic Client Registration Roadmap 2019/2020

Highlevel overview of the Conformance Tool and Dynamic Client Registration Roadmap for 2019/2020:


| Milestone 	                     | 7 Oct 	| 16 Oct 	| 26 Nov 	| 24 Dec 	| 29 Jan  	| 18 Mar 	|
|----------                          |---------- |-------	|--------	|--------	|---------- |-------	|
| Compatible with API Spec v3.1.2    | Beta      | Final    | 	        |	        |           |	        |
|  Dynamic Client Registration (DCR)  |           |	        | Beta (DCR)	    |v1.x (DCR)	    |           |	        | 
| CBPII Support                      |           | v2.x (FCS) 	|   	    |   	    |           |	        | 
| Compatible with API Spec v3.1.3    |           |	        |   	    |   	    |   v3.x (FCS)    |	        | 
| Compatible with API Spec v3.1.4    |           |	        |   	    |   	    |           |	  v3.x (FCS)  | 


Key:
* FCS: Functional Conformance Suite/Tool
* DCR: Dynamic Client Registration

